import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Button from "mt-strap/lib/es/Button/Button";

function App() {
  return (
    <div className="App">
      <Button variant={"blue"}>Hello World</Button>
    </div>
  );
}

export default App;
